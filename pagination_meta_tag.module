<?php

/**
 * @file
 * Contains pagination_meta_tag.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function pagination_meta_tag_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the pagination_meta_tag module.
    case 'help.page.pagination_meta_tag':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Adds the rel=&quot;next&quot; and rel=&quot;prev&quot; metatags when using pagination.') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_preprocess_pager().
 */
function pagination_meta_tag_preprocess_pager(&$pager) {
  $current_path = \Drupal\Core\Url::fromRoute('<current>');
  $path = $current_path->toString();

  // Correct for first page actually being page 0.
  $current_page = $pager['current'] - 1;
  // Parse the last page link to get the total number of pages.
  $total_pages = preg_replace('/^.*page=(\d+).*$/', '$1', $pager['items']['last']['href']);

  // Use the base path if on page 2 otherwise `page={{current_page-1}}`.
  $prev_href = ($current_page == 1 ? $path : ($current_page > 1 ? $path . '?page=' . ($current_page - 1) : NULL));
  $next_href = $current_page < $total_pages ? $path . '?page=' . ($current_page + 1) : NULL;

  // Add The prev rel link.
  if ($prev_href) {
    $pager['#attached']['html_head_link'][] = [
      [
        'rel' => 'prev',
        'href' => $prev_href,
      ],
      TRUE,
    ];
  }

  // Add the next rel link.
  if ($next_href) {
    $pager['#attached']['html_head_link'][] = [
      [
        'rel' => 'next',
        'href' => $next_href,
      ],
      TRUE,
    ];
  }
}
